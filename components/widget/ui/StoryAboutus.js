import React from "react";

const StoryAboutus = () => {
  return (
    <div
      className="flex items-center  justify-center mx-auto"
      style={{ backgroundColor: "#F87171" ,maxWidth: "40%" }}
    >
      <p className="text-white font-semibold uppercase mb-0">Welcome at GlobalHealthCities.com</p>
    </div>
  );
};

export default StoryAboutus;
