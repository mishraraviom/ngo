import React from "react";
import StoryAboutus from "../../../widget/ui/StoryAboutus";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { SliderCard } from "../../../widget/ui/Cards";

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 664, min: 0 },
    items: 1,
  },
};

const OurCauses = () => {
  return (
    <div className="items-center justify-center mt-10 md:p-16">
      <div>
        <div
          className="p-1 text-center mx-auto"
          style={{ backgroundColor: "#dbdbdb", maxWidth: "15%" }}
        >
          <p className="mb-0 text-white font-bold">Blogs</p>
        </div>
        <h2 className="uppercase text-center font-bold mt-3 "></h2>
      </div>
      <div className="mt-12 px-16">
        <Carousel
          swipeable={true}
          draggable={true}
          responsive={responsive}
          infinite={true}
          autoPlaySpeed={5000}
          keyBoardControl={true}
          customTransition="all .5"
          transitionDuration={500}
          autoPlay={true}
        >
          <SliderCard
            img="images/img-3.jpg"
            intro="In the middle of the current century, 70% of the world’s population will be living in urban areas. Today, many urban areas are already suffering from the negative effects of climate change as well as other negative consequences such as undermined social cohesion. "
            slideValue="30"
            Date=" July 21 2021"
            title="Desperate for Clean Water & Safe Water"
            percentage="30"
            status={true}
          />
          <SliderCard
            img="images/img-4.jpg"
            intro="In the middle of the current century, 70% of the world’s population will be living in urban areas. Today, many urban areas are already suffering from the negative effects of climate change as well as other negative consequences such as undermined social cohesion. "
            slideValue="20"
            Date=" July 21 2021"
            title="Desperate for Clean Water & Safe Water"
            percentage="20"
            status={true}
          />
          <SliderCard
            img="images/img-5.jpg"
            intro="In the middle of the current century, 70% of the world’s population will be living in urban areas. Today, many urban areas are already suffering from the negative effects of climate change as well as other negative consequences such as undermined social cohesion. "
            slideValue="35"
            Date=" July 21 2021"
            title="Want to create something meaningful?"
            percentage="35"
            status={true}
          />

          <SliderCard
            img="images/img-6.jpg"
            intro="In the middle of the current century, 70% of the world’s population will be living in urban areas. Today, many urban areas are already suffering from the negative effects of climate change as well as other negative consequences such as undermined social cohesion. "
            slideValue="60"
            Date=" July 21 2021"
            title="Desperate for Clean Water & Safe Water"
            percentage="60"
            status={true}
          />
          <SliderCard
            img="images/img-7.jpg"
            intro="In the middle of the current century, 70% of the world’s population will be living in urban areas. Today, many urban areas are already suffering from the negative effects of climate change as well as other negative consequences such as undermined social cohesion. "
            slideValue="70"
            Date=" July 21 2021"
            title="Desperate for Clean Water & Safe Water"
            percentage="70"
            status={true}
          />
        </Carousel>
      </div>
    </div>
  );
};

export default OurCauses;
